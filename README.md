# Spreadsheetfunction to Source Code

The goal of the project is to convert calculations in a Spreadsheet into source code in a programming language of choice. For the beginning the plan is to support the programming language R. This project is maybe helpful for Wikifunctions. 

## How to Install
Download the Repository as a Zip-File or as another option. Copy the downloaded compressed file and put it into a folder where you want it to be included. You can create a new folder or use an existing one. Decompress the file and put the decompressed content one level higher directly into the folder you want the result to be included. Edit the file Parsertop.R and change the Basepath to your Path of the folder you the folders Functionparsing and ODS-Filereader are located in. 
## How to use
Run the program Parsertop.R and look in the Folder Progoutputfiles. This is a subfolder of the Functionparsing folder. There you can find a program called Functionconvertresult.R. If you run this program you will get the Result in the Dataframe Baseframe.
## License
For open source projects, say how it is licensed.

