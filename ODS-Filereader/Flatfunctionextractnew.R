setwd(paste0(Basepath, "ODS-Filereader"))
#Progpath <- 'content.xml'
Filecontent <- scan("Examplefile20230409.fods", what="raw", encoding="UTF-8")
#Filecontent <- scan(Progpath, what="raw")
Filecontent <- data.frame(Filecontent)
Searchvar1 <- '<office:spreadsheet>'
Filecontent$Num <- 1
Filecontent$Num <- cumsum(Filecontent$Num)
Filecontent$Search <- substr(Filecontent$Filecontent, 1, 20)
Filecontent2 <- subset(Filecontent, Search == '<office:spreadsheet>')
#Filecontent$Start <- min(Filecontent2$Num)
Startrow <- grep("<office:spreadsheet>", Filecontent$Filecontent)
Startrow <- min(Startrow)
Filecontentendrow <- grep("</table:table>", Filecontent$Filecontent)
Filecontentendrow <- min(Filecontentendrow)
Filecontent$Start <- Startrow
Filecontent <- subset(Filecontent,Num > Start & Num < Filecontentendrow, select=c(Filecontent))
Filecontentstr <- paste(Filecontent$Filecontent, collapse=" ")
#Contrstrfurther$Contstrnumstart <- Contrstrfurther$Contstrnum + 1
Contstrnum <- rep(1, nchar(Filecontentstr))
Contstr <- data.frame(Contstrnum)
Contstr$Contstrnum <- cumsum(Contstr$Contstrnum)
Contstr$String <- Filecontentstr
Contstr$Sub <- substr(Contstr$String, Contstr$Contstrnum, Contstr$Contstrnum + 1)
Contstr$Sub2 <- substr(Contstr$String, Contstr$Contstrnum, Contstr$Contstrnum + 2)
Contrstrfurther <- subset(Contstr, Sub == '><' | Sub2 == '> <')
Contrstrfurther$Contstrnumstart <- Contrstrfurther$Contstrnum + 1
Contrstrfurther$End <- c(Contrstrfurther[2:nrow(Contrstrfurther), 1] , nchar(Filecontentstr))
Contrstrfurther$Sub <- substr(Contrstrfurther$String, Contrstrfurther$Contstrnum + 1, Contrstrfurther$End)
Contrstrfurther$Filecontent <- Contrstrfurther$Sub
Filecontent <- subset(Contrstrfurther, select=c(Filecontent))
Filecontent$Sub <- ifelse(substr(Filecontent$Filecontent, 1, 1) == ' ', substr(Filecontent$Filecontent, 2, nchar(Filecontent$Filecontent)), Filecontent$Filecontent)
Filecontent$Filecontent <- Filecontent$Sub
Filecontent$Filecontentsub2 <- substr(Filecontent$Filecontent, 1, 16)
Filecontent <- subset(Filecontent, select=c(Filecontent, Filecontentsub2))
Filecontent$Cellstart <- ifelse(Filecontent$Filecontent == '<table:table-cell/>' | substr(Filecontent$Filecontent, 1, 17) == '<table:table-cell' | substr(Filecontent$Filecontent, 1, 25) == '<table:covered-table-cell' , 1, 0)
Filecontent$Cellstart2 <- cumsum(Filecontent$Cellstart)
Filecontent$Cellend <- ifelse(Filecontent$Filecontent == '<table:table-cell/>' | Filecontent$Filecontent == '</table:table-cell>' | Filecontent$Cellstart == 1 & substr(Filecontent$Filecontent, nchar(Filecontent$Filecontent) - 1, nchar(Filecontent$Filecontent)) == '/>', 1, 0)
Filecontent$Rowstart <- ifelse(Filecontent$Filecontentsub2 == '<table:table-row', 1, 0)
Filecontent$Rowstart2 <- cumsum(Filecontent$Rowstart)
Formulapos <- grep('table:formula="', Filecontent$Filecontent)
Filecontent$Posnum <- 1
Filecontent$Posnum <- cumsum(Filecontent$Posnum)
Filecontent$Textpos <- grepl('<text:p>', Filecontent$Filecontent)
Textposframecalc <- nrow(subset(Filecontent, Textpos == TRUE, select=c(Textpos, Posnum, Filecontent)))
ifelse(Textposframecalc > 0, source(paste0(Basepath, "ODS-Filereader/Texttagextrakt.R")),'')
Filecontent$Textpos2 <- grepl('table:formula=', Filecontent$Filecontent)
Formulaposframecalc <- nrow(subset(Filecontent, Textpos2 == TRUE, select=c(Textpos2, Posnum, Filecontent)))
ifelse(Formulaposframecalc > 0, source(paste0(Basepath, "ODS-Filereader/Formulatextextrakt.R")),source(paste0(Basepath, "ODS-Filereader/Nonformulaposframe.R")))
Filecontent$Rowpos <- grepl('table:number-rows-repeated', Filecontent$Filecontent)
Rowposframecalc <- nrow(subset(Filecontent, Rowpos == TRUE, select=c(Textpos, Posnum, Filecontent)))
ifelse(Rowposframecalc > 0, source(paste0(Basepath, "ODS-Filereader/Rownumrepeatextraction.R")),source(paste0(Basepath, "ODS-Filereader/Nonrowposframeconstructor.R")))
Filecontent$Colpos <- grepl('table:number-columns-repeated', Filecontent$Filecontent)
Colposframecalc <- nrow(subset(Filecontent, Colpos == TRUE, select=c(Textpos, Posnum, Filecontent)))
ifelse(Colposframecalc > 1, source(paste0(Basepath, "ODS-Filereader/Colnumrepeatextraction.R")),source(paste0(Basepath, "ODS-Filereader/Noncolposframecreator.R")))
Colposcorr <- sum(as.numeric(Colposframe$Subbaseres3)) - nrow(Colposframe)
Colposcorr <- ifelse(Colposcorr < 0, 0, Colposcorr)
Cellnumber <- max(Filecontent$Cellstart2) + Colposcorr
Rowsubnumber <- Cellnumber / max(Filecontent$Rowstart2)
Colsubnumber2209 <- Rowsubnumber
Cellnumberadd2 <- sum(as.numeric(Rowposframe$Subbaseres2)) * Rowsubnumber - nrow(Rowposframe) * Rowsubnumber 
Cellnumberadd <- ifelse(Cellnumberadd2 < 0, 0, Cellnumberadd2)
Cellnumberreal <- Cellnumber + Cellnumberadd
Rownumberreal <- Cellnumberreal / Colsubnumber2209
source(paste0(Basepath, "ODS-Filereader/Baseframeconstructor.R"))
print('Zellenzahl in Cellnumber und Cellnumberreal, Zeilenzahl in Rownumberreal')
