#Processingsubpart <- subset(Processingsubset, Cellposname == 'C5' & Level2 == 0)
#Cellpossubname <- subset(Processingsubpart, Duplicated == FALSE, select=Cellposname)
Indexbase <- paste0('Indexframe <- ',Processingsubsetfurther$Varoptsres[2])
Indexfuncres <- paste0(Procsubargframe$Argdest[Procsubargframenum],' <- Indexframe[',Processingsubsetfurther$Varoptsres[3],', ', Processingsubsetfurther$Varoptsres[4], ']')
Lines <- c(Indexbase, Indexfuncres)
source(paste0(Basepath, "Functionparsing/Functionwriter.R"))
Sourceoutputlevel <- Procsubargframe[Procsubargframenum, 1]
Procsubargframenum <- Procsubargframenum + 1
ifelse(Procsubargframenum > max(Procsubargframe$Num), 'kein weiterer Frame', source(paste0(Basepath, "Functionparsing/Functionconstructselection.R")))
