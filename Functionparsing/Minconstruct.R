#Processingsubpart <- subset(Processingsubset, Cellposname == 'C5' & Level2 == 0)
#Cellpossubname <- subset(Processingsubpart, Duplicated == FALSE, select=Cellposname)
Lengthres <- paste0(Procsubargframe$Argdest[Procsubargframenum],' <- min(as.numeric(',Processingsubsetfurther$Varoptsres[2] ,'))')
Lines <- Lengthres
source(paste0(Basepath, "Functionparsing/Functionwriter.R"))
Sourceoutputlevel <- Procsubargframe[Procsubargframenum, 1]
Procsubargframenum <- Procsubargframenum + 1
ifelse(Procsubargframenum > max(Procsubargframe$Num), 'kein weiterer Frame', source(paste0(Basepath, "Functionparsing/Functionconstructselection.R")))
