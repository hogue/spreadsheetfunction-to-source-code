#Substrstartbackup$Calculationlevel <- 0
Cellposcalclist <- subset(Substrstartbackup, Level2 == Calculationlevel & Lowestrownew == 0 & Valoutput > 0, select=c(Valoutput, Cellposname))
#Cellposcalclist <- subset(Substrstartbackup, Level2 == 0 & Lowestrownew == 0 & Valoutput > 0, select=c(Cellposname, Valoutput))
#Cellposcalclist$Cellposname <- Cellposcalclist$Cellposition
Cellposcalclist$Linenum <- 1
Cellposcalclist$Linenum <- cumsum(Cellposcalclist$Linenum)
Cellposcalclist <- subset(Cellposcalclist, select=c(Cellposname, Linenum))
SubstrstartbackupV2 <- subset(Substrstartbackup, select=c(Valoutput, Level2, Rownum))
Substrstartbackup$Cellpos <- ifelse(Substrstartbackup$Valoutput == 0 | Substrstartbackup$Level2 != Substrstartbackup$Calculationlevel + 1 , '',1)
SubstrstartbackupV2 <- merge(SubstrstartbackupV2, Cellposcalclist, by.x="Valoutput", by.y="Linenum", all.x=TRUE)
SubstrstartbackupV2 <- SubstrstartbackupV2[order(SubstrstartbackupV2$Rownum),]
Substrstartbackup$Cellposprename <- SubstrstartbackupV2$Cellposname
Substrstartbackup$Cellposname <- ifelse(Substrstartbackup$Cellpos == 1, Substrstartbackup$Cellposprename, Substrstartbackup$Cellposname)
Substrstartbackup$Duplicated <- duplicated(paste0(Substrstartbackup$Level2, Substrstartbackup$Valoutput, Substrstartbackup$Cellpos))
Substrstartbackup$Calculationlevel <- Substrstartbackup$Calculationlevel + 1
Cellposprogdec <- ifelse(max(Substrstartbackup$Level2) > max(Substrstartbackup$Calculationlevel), source("Cellposcalclist.R"), 0)