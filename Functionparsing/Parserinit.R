#Funktion <- readline('Enter function name: ')
#Cellposition <- readline('Enter Cellposition: ')
Reladdressframe <- subset(Cellbaseresult, Sub2 != '', select=c(Sub2, Celladdress, Celladdressnumnotation))
Funktion <- Rangestr$Sub
#Funktion <- c('=C3','=SVERWEIS(A3;H:K;4;0)', Rangestr$Sub)
#Funktion <- c('=F2', '=TEIL(C2;3;8)')
#Cellposition <- c('E2', 'H2')
Cellposition <- c('A1048577', 'B1048577', 'C1048577', Reladdressframe$Celladdress)
#Cellposition <- c('E2','G2', 'A20', 'B20', 'C20', Reladdressframe$Celladdress)
Cellposlist <- data.frame(Cellposition)
Funktionsliste <- data.frame(Funktion)
Funktionsbasis <- Funktionsliste
Funktionsliste$Length <- nchar(Funktionsliste$Funktion)
Level <- 0
Sub <- 0
Substr <- 0
Valoutput <- 0
Functionkind <- 0 
Cellkindres <- 0
Lowestrownew <- 0
Level2 <- 0
Substrstartbackup <- data.frame(Sub, Substr, Valoutput, Functionkind, Cellkindres, Lowestrownew, Level2)
source(paste0(Basepath, "Functionparsing/Parserdemonstrationalt.R"))
Substrstartbackup$Rownum <- 1
Substrstartbackup$Rownum <- cumsum(Substrstartbackup$Rownum)
Cellposlist$Linenum <- 1
Cellposlist$Linenum <- cumsum(Cellposlist$Linenum)
SubstrstartbackupV2 <- subset(Substrstartbackup, select=c(Valoutput, Level2, Rownum))
Substrstartbackup$Cellpos <- ifelse(Substrstartbackup$Valoutput == 0 | Substrstartbackup$Level2 > 0, '',1)
SubstrstartbackupV2 <- merge(SubstrstartbackupV2, Cellposlist, by.x="Valoutput", by.y="Linenum", all.x=TRUE)
SubstrstartbackupV2 <- SubstrstartbackupV2[order(SubstrstartbackupV2$Rownum),]
Substrstartbackup$Cellposprename <- SubstrstartbackupV2$Cellposition
Substrstartbackup$Cellposname <- ifelse(Substrstartbackup$Cellpos == 1, Substrstartbackup$Cellposprename, '')
Substrstartbackup$Duplicated <- duplicated(paste0(Substrstartbackup$Level2, Substrstartbackup$Valoutput, Substrstartbackup$Cellpos))
Substrfollowcalc <- subset(Substrstartbackup, Valoutput > 0 & Lowestrownew == 0)
Substrstartbackup$Calculationlevel <- 0
Decprogcheck <- ifelse(max(Substrstartbackup$Level2) == 0, '', source("Cellposcalclist.R"))
print('Ergebnis in Substrstartbackup')