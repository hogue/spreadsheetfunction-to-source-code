Ranges <- Processingsubsetfurther$Varoptsres[1:nrow(Processingsubsetfurther)]
Ranges2 <- Processingsubsetfurther$Substralt[2:nrow(Processingsubsetfurther)]
Ranges2 <- ifelse(Ranges2 == '=', '==', Ranges2)
Ranges2frame <- data.frame(Ranges2)
Rangesframe <- data.frame(Ranges)
Ranges2rowstat <- ifelse(nrow(subset(Ranges2frame, Ranges2 == ',')) > 0, 1, 0)
Ranges2rowstat2 <- ifelse(nrow(subset(Ranges2frame, Ranges2 == '+' | Ranges2 == '-' | Ranges2 == '*' | Ranges2 == '/' | Ranges2 == '^')) == nrow(Ranges2frame), 1, 0)
Rangesframe$Ranges2rowstat <- Ranges2rowstat
Rangesframe$Ranges3rowstat <- nrow(subset(Ranges2frame, Ranges2 == '!=' | Ranges2 == '=='))
Rangesframe$Check <- ifelse(Rangesframe$Ranges3rowstat > 0, Rangesframe$Ranges, paste0('as.numeric(',Rangesframe$Ranges,')'))
Ranges <- Rangesframe$Check
Ranges2 <- c('paste0(', Ranges2,')')
Rangesres <- c(Ranges, Ranges2)
Rangesres <- data.frame(Rangesres)
Rangesresnum <- c(cumsum(rep(2, length(Ranges))), cumsum(c(1, rep(2, length(Ranges2) - 1))))
Rangesres$Rangesresnum <- Rangesresnum
Rangesres <- Rangesres[order(Rangesres$Rangesresnum),]
Opfuncoutput <- paste(Rangesres$Rangesres, collapse="")
Opfuncoutput <- ifelse(Ranges2rowstat2 == 1, substr(Opfuncoutput, 8, nchar(Opfuncoutput) - 1), Opfuncoutput)
Opfuncoutput <- paste0(Procsubargframe$Argdest[Procsubargframenum],' <- ',Opfuncoutput)
Lines <- Opfuncoutput
source(paste0(Basepath, "Functionparsing/Functionwriter.R"))
print('Teilergebnis in Opfuncoutput')
Sourceoutputlevel <- Procsubargframe[Procsubargframenum, 1]
Procsubargframenum <- Procsubargframenum + 1
ifelse(Procsubargframenum > max(Procsubargframe$Num), 'kein weiterer Frame', source(paste0(Basepath, "Functionparsing/Functionconstructselection.R")))
