Countnum <- 1
Cellbaseresultfurther <- subset(Cellbaseresult, Sub2 != '', select=c(Celladdressnumnotation, Celladdress))
Baseframesub <- Cellbaseresultfurther$Celladdressnumnotation
Processingsubset2 <- merge(Processingsubset, Cellbaseresultfurther, by.x="Cellposname", by.y="Celladdress", all.x=TRUE)
Procsubaggregateres <- Cellbaseresultfurther
Procsubaggregateres$Matchcellpos <- Procsubaggregateres$Celladdressnumnotation
Procsubaggregateres$Matchcelldecisionnum <- 1
Procsubaggregateres$Countnumres <- 0
Procsubaggregateres <- subset(Procsubaggregateres, select=c(Matchcellpos, Matchcelldecisionnum, Countnumres))
source(paste0(Basepath, "Functionparsing/Relationrowprocessing.R"))
Procsubaggregateres$Max <- Countnum - 1
Procsubaggregateres2 <- subset(Procsubaggregateres, Matchcelldecisionnum > 0 & Countnumres < Max, select=c(Matchcellpos, Matchcelldecisionnum, Countnumres))
Res <- aggregate(Procsubaggregateres2$Countnumres, list(Procsubaggregateres2$Matchcellpos), FUN=max)
Res$Level <- Res$x
Res$Address <- Res$Group.1
Res <- Res[order(Res$Level),]
Res <- subset(Res, select=c(Address, Level))
Resbackup <- Res
Res$Ordernum <- 1
Res$Ordernum <- cumsum(Res$Ordernum)
Res <- merge(Res, Procsubargframe, by.x="Address", by.y="Route", all.x=TRUE)
Res <- Res[order(Res$Ordernum),]
Resbackup20221117 <- Res
Resbackup20221117$Ordernum2 <- Resbackup20221117$Ordernum * 1000000 - as.numeric(substr(Resbackup20221117$Procsubsetsrcharg, 1, 1)) * 100000 - as.numeric(substr(Resbackup20221117$Procsubsetsrcharg, 3, nchar(Resbackup20221117$Procsubsetsrcharg)))
Resbackup20221117 <- Resbackup20221117[order(Resbackup20221117$Ordernum2),]
Res <- Resbackup20221117
print('Ergebnis in Resbackup20221117')
